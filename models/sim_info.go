package models

import (
	"time"
)

type SimInfoModel struct {
	Imei      uint64
	SimId     int32
	Mcc       int32
	Mnc       int32
	Phone     string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

func (SimInfoModel) TableName() string {
	return "sim_info"
}
