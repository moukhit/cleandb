package models

import (
	"time"

	"github.com/moukhit/sltcp"
)

type DeviceModel struct {
	Imei             uint64 `gorm:"primaryKey"`
	SerialNumber1    string `gorm:"column:serial_number_1"`
	DeviceType       sltcp.DeviceTypeEnum
	Tel              string
	Owner            string `gorm:"column:device_owner"`
	Firmware         string `gorm:"size:10"`
	SerialNumber2    string `gorm:"column:serial_number_2"`
	Modules          string
	VersionMajor     int32
	VersionMinor     int32
	VersionRevision  int32
	VersionPatch     int32
	VersionTest      bool
	VoltageLimit     int32
	BalanceSim1Limit int32
	BalanceSim2Limit int32
	EngStartPermit   sltcp.EngStartPermit
	WebastoPermit    sltcp.WebastoPermit
	GitHash          string
	UidHash          string
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        time.Time
}

func (DeviceModel) TableName() string {
	return "devices"
}
