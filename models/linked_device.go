package models

import (
	"time"

	"github.com/moukhit/sltcp"
)

type LinkedDeviceModel struct {
	Imei          uint64
	DeviceId      uint32                   `gorm:"column:linked_device_id"`
	DeviceType    sltcp.LinkDeviceTypeEnum `gorm:"column:linked_device_type"`
	SerialNumber  string                   `gorm:"size:10"`
	TagDetectTime uint32
	TagLostTime   uint32
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     time.Time
}

func (LinkedDeviceModel) TableName() string {
	return "linked_devices"
}
