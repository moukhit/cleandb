package interfaces

import (
	"gorm.io/gorm"
)

type DbConnector interface {
	OpenConnection(connectionString string) (*gorm.DB, error)
	GetDBInstance() *gorm.DB
	CloseConnection() error
}
