package interfaces

import (
	"cleandb/models"

	"github.com/moukhit/sltcp"
)

type DeviceRepositiry interface {
	AddOrUpdate(model *models.DeviceModel) error
	Delete(imei uint64) error
	GetByImei(imei uint64) (models.DeviceModel, error)
}

type LinkedDeviceRepositiry interface {
	AddOrUpdate(model *models.LinkedDeviceModel) error
	Delete(imei uint64, device_id uint32, device_type sltcp.LinkDeviceTypeEnum) error
	Find(imei uint64, device_id uint32, device_type sltcp.LinkDeviceTypeEnum) (models.LinkedDeviceModel, error)
}

type SimInfoRepository interface {
	AddOrUpdate(model *models.SimInfoModel) error
	Delete(imei uint64, simid int32, mcc int32, mnc int32) error
	Find(imei uint64, simid int32, mcc int32, mnc int32) error
}
