package main

import "fmt"

const (
	DBUser     = "postgres"
	DBPassword = "postgres"
	DBName     = "starline"
	DBHost     = "localhost"
	DBPort     = "5432"
	DBType     = "postgres"
)

type Config struct{}

func (cfg *Config) GetDBType() string {
	return DBType
}

func (cfg *Config) GetPostgreSQLConnectionString() string {
	connstr := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		DBHost,
		DBPort,
		DBUser,
		DBName,
		DBPassword)
	return connstr
}
