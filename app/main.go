package main

import (
	"cleandb/models"
	"cleandb/repositories/postgres"
	"cleandb/tests"
	"fmt"
	"os"
)

func main() {
	pAuthRequest, err := tests.ParseAuthRequestMessage()
	if err != nil {
		os.Exit(1)
	}

	config := Config{}
	connString := config.GetPostgreSQLConnectionString()

	conn := postgres.PostgreSQLConnector{}
	db, err := conn.OpenConnection(connString)
	if err != nil {
		fmt.Printf("Ошибка при подключении к базе данных: %v", err)
		os.Exit(2)
	}
	defer conn.CloseConnection()

	repoDevice := postgres.CreateDeviceRepository(db)

	device := models.DeviceModel{
		Imei:             uint64(pAuthRequest.GetId()),
		SerialNumber1:    "900-00280 F012 081281",
		DeviceType:       pAuthRequest.GetType(),
		Tel:              "+77005333988",
		Owner:            "+77772235001",
		Firmware:         pAuthRequest.GetFrm(),
		SerialNumber2:    pAuthRequest.GetSn(),
		VersionMajor:     *pAuthRequest.GetVersion().Major,
		VersionMinor:     *pAuthRequest.GetVersion().Minor,
		VersionRevision:  *pAuthRequest.GetVersion().Revision,
		VersionPatch:     *pAuthRequest.GetVersion().Patch,
		VersionTest:      *pAuthRequest.GetVersion().Test,
		VoltageLimit:     pAuthRequest.GetVoltageLimit(),
		BalanceSim1Limit: pAuthRequest.GetBalanceSim1Limit(),
		BalanceSim2Limit: pAuthRequest.GetBalanceSim2Limit(),
		EngStartPermit:   pAuthRequest.GetEngStartPermit(),
		WebastoPermit:    pAuthRequest.GetWebastoPermit(),
		GitHash:          pAuthRequest.GetGitHash(),
		UidHash:          pAuthRequest.GetUidHash(),
	}

	if err = repoDevice.AddOrUpdate(&device); err != nil {
		fmt.Printf("Ошибка при добавлении или изменении записи: %v", err)
		println()
		os.Exit(3)
	}

	if len(pAuthRequest.Devices) > 0 {
		repoLinkedDevice := postgres.CreateLinkedDeviceRepository(db)

		for _, ld := range pAuthRequest.Devices {
			linked_device := models.LinkedDeviceModel{
				Imei:          device.Imei,
				DeviceId:      ld.GetId(),
				DeviceType:    ld.GetType(),
				SerialNumber:  ld.GetSn(),
				TagDetectTime: ld.GetTagDetectTime(),
				TagLostTime:   ld.GetTagLostTime(),
			}

			if err = repoLinkedDevice.AddOrUpdate(&linked_device); err != nil {
				fmt.Printf("Ошибка при добавлении или изменении записи о связанном устройстве: %v", err)
				println()
				os.Exit(3)
			}
		}
	}

	if len(pAuthRequest.Sims) > 0 {
		repoSimInfo := postgres.CreateSimInfoRepository(db)

		for _, si := range pAuthRequest.Sims {
			sim_info := models.SimInfoModel{
				Imei:  device.Imei,
				SimId: si.GetId(),
				Mcc:   si.GetMcc(),
				Mnc:   si.GetMnc(),
				Phone: si.GetPhone(),
			}

			if err = repoSimInfo.AddOrUpdate(&sim_info); err != nil {
				fmt.Printf("Ошибка при добавлении или изменении записи о сим-карте: %v", err)
				println()
				os.Exit(3)
			}
		}
	}

	println("Done.")
}
