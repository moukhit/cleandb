package tests

import (
	"cleandb/models"
	"cleandb/repositories/postgres"

	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CreateDeviceRepository(t *testing.T) {
	config = Config{}
	connString = config.GetPostgreSQLConnectionString()

	conn = postgres.PostgreSQLConnector{}
	db, err := conn.OpenConnection(connString)

	assert.Nil(t, err, "Ошибка при подключении к базе данных: %v", err)
	defer conn.CloseConnection()

	pRepoDevice = postgres.CreateDeviceRepository(db)
	assert.NotNil(t, pRepoDevice, "Репозиторий DeviceRepository не создан.")
}

func Test_AddOrUpdate(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pAuthRequest, err = ParseAuthRequestMessage()
	assert.Nil(t, err, "ошибка при парсинге пакета авторизации")

	device := models.DeviceModel{
		Imei:             uint64(pAuthRequest.GetId()),
		SerialNumber1:    "900-00280 F012 081281",
		DeviceType:       pAuthRequest.GetType(),
		Tel:              "+77005333988",
		Owner:            "+77772235001",
		Firmware:         pAuthRequest.GetFrm(),
		SerialNumber2:    pAuthRequest.GetSn(),
		VersionMajor:     *pAuthRequest.GetVersion().Major,
		VersionMinor:     *pAuthRequest.GetVersion().Minor,
		VersionRevision:  *pAuthRequest.GetVersion().Revision,
		VersionPatch:     *pAuthRequest.GetVersion().Patch,
		VersionTest:      *pAuthRequest.GetVersion().Test,
		VoltageLimit:     pAuthRequest.GetVoltageLimit(),
		BalanceSim1Limit: pAuthRequest.GetBalanceSim1Limit(),
		BalanceSim2Limit: pAuthRequest.GetBalanceSim2Limit(),
		EngStartPermit:   pAuthRequest.GetEngStartPermit(),
		WebastoPermit:    pAuthRequest.GetWebastoPermit(),
		GitHash:          pAuthRequest.GetGitHash(),
		UidHash:          pAuthRequest.GetUidHash(),
	}

	pRepoDevice = postgres.CreateDeviceRepository(db)
	err = pRepoDevice.AddOrUpdate(&device)
	assert.Nil(t, err, "ошибка при добавлении или изменении записи в таблицу devices")
}

func Test_Delete(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	var imei uint64 = 868328052162290

	pRepoDevice = postgres.CreateDeviceRepository(db)
	err = pRepoDevice.Delete(imei)

	assert.Nil(t, err, "ошибка при удалении записи из таблицы devices")
}

func Test_GetByImei(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	var (
		imei    uint64 = 868328052162290
		pDevice *models.DeviceModel
	)

	pRepoDevice = postgres.CreateDeviceRepository(db)
	pDevice, err = pRepoDevice.GetByImei(imei)
	assert.Nil(t, err, "ошибка при выборе устройства по его IMEI")
	assert.NotNil(t, pDevice, "не найдено устройство по заданному IMEI")
	assert.Equal(t, imei, pDevice.Imei)

	imei = 1234567890
	pDevice, err = pRepoDevice.GetByImei(imei)
	assert.NotNil(t, err)
	assert.Equal(t, uint64(0), pDevice.Imei)
}
