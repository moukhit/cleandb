package tests

import (
	"cleandb/repositories/postgres"

	"github.com/moukhit/sltcp"
	"gorm.io/gorm"
)

var (
	pAuthRequest      *sltcp.AuthRequest
	pRepoDevice       *postgres.PostgresDeviceRepository
	pRepoLinkedDevice *postgres.PostgresLinkedDeviceRepository
	pRepoSimInfo      *postgres.PostgresSimInfoRepository
	config            Config
	connString        string
	conn              postgres.PostgreSQLConnector
	db                *gorm.DB
	err               error
)

func InitTest() {
	config = Config{}
	connString := config.GetPostgreSQLConnectionString()

	conn = postgres.PostgreSQLConnector{}
	db, err = conn.OpenConnection(connString)
}
