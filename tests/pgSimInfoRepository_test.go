package tests

import (
	"cleandb/models"
	"cleandb/repositories/postgres"

	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CreateSimInfoRepository(t *testing.T) {
	config = Config{}
	connString = config.GetPostgreSQLConnectionString()

	conn = postgres.PostgreSQLConnector{}
	db, err := conn.OpenConnection(connString)

	assert.Nil(t, err, "Ошибка при подключении к базе данных: %v", err)
	defer conn.CloseConnection()

	pRepoSimInfo = postgres.CreateSimInfoRepository(db)
	assert.NotNil(t, pRepoSimInfo, "Репозиторий PostgresSimInfoRepository не создан.")
}

func Test_AddOrUpdateSimInfo(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pAuthRequest, err = ParseAuthRequestMessage()
	assert.Nil(t, err, "ошибка при парсинге пакета авторизации")
	if len(pAuthRequest.Devices) > 0 {
		pRepoSimInfo = postgres.CreateSimInfoRepository(db)

		for _, s := range pAuthRequest.Sims {
			sim := models.SimInfoModel{
				Imei:  uint64(pAuthRequest.GetId()),
				SimId: s.GetId(),
				Mcc:   s.GetMcc(),
				Mnc:   s.GetMnc(),
				Phone: s.GetPhone(),
			}

			sim.Phone = "+77772235001"
			if err = pRepoSimInfo.AddOrUpdate(&sim); err != nil {
				assert.Nil(t, err)
			}
		}
	}
}

func Test_DeleteSimInfo(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pRepoSimInfo = postgres.CreateSimInfoRepository(db)

	var (
		imei  uint64 = 868328052162290
		simid int32  = 2
		mcc   int32  = 401
		mnc   int32  = 770
	)

	err = pRepoSimInfo.Delete(imei, simid, mcc, mnc)
	assert.Nil(t, err)

	mnc = 77
	err = pRepoSimInfo.Delete(imei, simid, mcc, mnc)
	assert.Nil(t, err)
}

func Test_FindSimInfo(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pRepoSimInfo = postgres.CreateSimInfoRepository(db)

	var (
		imei   uint64 = 868328052162290
		sim_id int32  = 2
		mcc    int32  = 401
		mnc    int32  = 77
	)

	sim_info, err := pRepoSimInfo.Find(imei, sim_id, mcc, mnc)
	assert.Nil(t, err)
	assert.Equal(t, imei, sim_info.Imei)

	mcc = 770
	sim_info, err = pRepoSimInfo.Find(imei, sim_id, mcc, mnc)
	assert.NotNil(t, err)
	assert.Equal(t, uint64(0), sim_info.Imei)
}
