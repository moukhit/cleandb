package tests

import (
	"cleandb/models"
	"cleandb/repositories/postgres"

	"testing"

	"github.com/moukhit/sltcp"
	"github.com/stretchr/testify/assert"
)

func Test_CreateLinkedDeviceRepository(t *testing.T) {
	config = Config{}
	connString = config.GetPostgreSQLConnectionString()

	conn = postgres.PostgreSQLConnector{}
	db, err := conn.OpenConnection(connString)

	assert.Nil(t, err, "Ошибка при подключении к базе данных: %v", err)
	defer conn.CloseConnection()

	pRepoLinkedDevice = postgres.CreateLinkedDeviceRepository(db)
	assert.NotNil(t, pRepoLinkedDevice, "Репозиторий LinkedDeviceRepository не создан.")
}

func Test_AddOrUpdateLinkedDevice(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pAuthRequest, err = ParseAuthRequestMessage()
	assert.Nil(t, err, "ошибка при парсинге пакета авторизации")
	if len(pAuthRequest.Devices) > 0 {
		pRepoLinkedDevice = postgres.CreateLinkedDeviceRepository(db)

		for _, ld := range pAuthRequest.Devices {
			linked_device := models.LinkedDeviceModel{
				Imei:          uint64(pAuthRequest.GetId()),
				DeviceId:      ld.GetId(),
				DeviceType:    ld.GetType(),
				SerialNumber:  ld.GetSn(),
				TagDetectTime: ld.GetTagDetectTime(),
				TagLostTime:   ld.GetTagLostTime(),
			}

			if err = pRepoLinkedDevice.AddOrUpdate(&linked_device); err != nil {
				assert.Nil(t, err)
			}
		}
	}
}

func Test_DeleteLinkedDevice(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pRepoLinkedDevice = postgres.CreateLinkedDeviceRepository(db)

	var (
		imei        uint64                   = 868328052162290
		device_id   uint32                   = uint32(10)
		device_type sltcp.LinkDeviceTypeEnum = sltcp.LinkDeviceTypeEnum(2)
	)

	err = pRepoLinkedDevice.Delete(imei, device_id, device_type)
	assert.Nil(t, err)
}

func Test_FindLinkedDevice(t *testing.T) {
	InitTest()
	defer conn.CloseConnection()

	pRepoLinkedDevice = postgres.CreateLinkedDeviceRepository(db)

	var (
		imei        uint64                   = 868328052162290
		device_id   uint32                   = uint32(10)
		device_type sltcp.LinkDeviceTypeEnum = sltcp.LinkDeviceTypeEnum(2)
	)

	linked_device, err := pRepoLinkedDevice.Find(imei, device_id, device_type)
	assert.Nil(t, err)
	assert.Equal(t, imei, linked_device.Imei)

	device_type = sltcp.LinkDeviceTypeEnum(3)
	linked_device, err = pRepoLinkedDevice.Find(imei, device_id, device_type)
	assert.NotNil(t, err)
	assert.Equal(t, uint64(0), linked_device.Imei)
}
