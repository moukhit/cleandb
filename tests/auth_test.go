package tests

import (
	"testing"

	"github.com/moukhit/sltcp"
	"github.com/stretchr/testify/assert"
)

func Test_AuthRequest(t *testing.T) {
	var (
		p   *sltcp.AuthRequest
		err error
	)

	p, err = ParseAuthRequestMessage()

	assert.Nil(t, err, "cannot parse message")
	assert.Equal(t, int64(868328052162290), p.GetId())
	assert.Equal(t, sltcp.DeviceTypeEnum(22), p.GetType())
	assert.Equal(t, "2.24.2", p.GetFrm())
	assert.Equal(t, "ES96 S012 087198", p.GetSn())
	assert.Equal(t, int32(2), *p.GetVersion().Major)
	assert.Equal(t, int32(24), *p.GetVersion().Minor)
	assert.Equal(t, int32(12266), *p.GetVersion().Revision)
	assert.Equal(t, int32(2), *p.GetVersion().Patch)
	assert.Equal(t, false, *p.GetVersion().Test)
	assert.Equal(t, int32(12000), p.GetVoltageLimit())
	assert.Equal(t, int32(20), p.GetBalanceSim1Limit())
	assert.Equal(t, sltcp.EngStartPermit(0), p.GetEngStartPermit())
	assert.Equal(t, sltcp.WebastoPermit(1), p.GetWebastoPermit())
	assert.Equal(t, "cfc33c1bc1", p.GetGitHash())
	assert.Equal(t, "0f90c9ec9bc5b01342f7543a36683230", p.GetUidHash())

	assert.Equal(t, 1, len(p.Devices))
	for i, d := range p.Devices {
		switch i {
		case 0:
			assert.Equal(t, uint32(10), d.GetId())
			assert.Equal(t, sltcp.LinkDeviceTypeEnum(2), d.GetType())
		}
	}
}
