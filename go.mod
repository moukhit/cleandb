module cleandb

go 1.16

replace github.com/moukhit/sltcp => ../sltcp-parser

require (
	github.com/jackc/pgx/v4 v4.11.0 // indirect
	github.com/moukhit/sltcp v0.0.0-00010101000000-000000000000
	github.com/stretchr/testify v1.5.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.6
)
