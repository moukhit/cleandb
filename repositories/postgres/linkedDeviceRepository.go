package postgres

import (
	"cleandb/models"
	"errors"

	"github.com/moukhit/sltcp"
	"gorm.io/gorm"
)

type PostgresLinkedDeviceRepository struct {
	DB *gorm.DB
}

func CreateLinkedDeviceRepository(db *gorm.DB) *PostgresLinkedDeviceRepository {
	return &PostgresLinkedDeviceRepository{DB: db}
}

func (repo *PostgresLinkedDeviceRepository) AddOrUpdate(model *models.LinkedDeviceModel) error {
	if repo.DB == nil {
		return errors.New("подключение к базе данных не инициализировано")
	}

	var (
		result        *gorm.DB
		linked_device *models.LinkedDeviceModel
	)

	result = repo.DB.Where(&models.LinkedDeviceModel{
		Imei:       model.Imei,
		DeviceId:   model.DeviceId,
		DeviceType: model.DeviceType,
	}).First(&linked_device)

	if result.Error == gorm.ErrRecordNotFound {
		result = repo.DB.Create(model)
	} else {
		linked_device.DeviceId = model.DeviceId
		linked_device.DeviceType = model.DeviceType
		linked_device.SerialNumber = model.SerialNumber
		linked_device.TagDetectTime = model.TagDetectTime
		linked_device.TagLostTime = model.TagLostTime

		result = repo.DB.Where(&models.LinkedDeviceModel{
			Imei:       model.Imei,
			DeviceId:   model.DeviceId,
			DeviceType: model.DeviceType,
		}).Save(&linked_device)
	}

	return result.Error
}

func (repo *PostgresLinkedDeviceRepository) Delete(imei uint64, device_id uint32, device_type sltcp.LinkDeviceTypeEnum) error {
	if repo.DB == nil {
		return errors.New("подключение к базе данных не инициализировано")
	}

	result := repo.DB.Delete(&models.LinkedDeviceModel{}, "imei=? and linked_device_id=? and linked_device_type=?", imei, device_id, device_type)
	return result.Error
}

func (repo *PostgresLinkedDeviceRepository) Find(imei uint64, device_id uint32, device_type sltcp.LinkDeviceTypeEnum) (*models.LinkedDeviceModel, error) {
	if repo.DB == nil {
		return nil, errors.New("подключение к базе данных не инициализировано")
	}

	var (
		result        *gorm.DB
		linked_device *models.LinkedDeviceModel
	)

	result = repo.DB.Where(&models.LinkedDeviceModel{
		Imei:       imei,
		DeviceId:   device_id,
		DeviceType: device_type,
	}).First(&linked_device)

	return linked_device, result.Error
}
