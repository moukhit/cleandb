package postgres

import (
	"database/sql"
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type PostgreSQLConnector struct {
	db *gorm.DB
}

func (c *PostgreSQLConnector) OpenConnection(connectionString string) (*gorm.DB, error) {
	var err error

	c.db, err = gorm.Open(postgres.Open(connectionString), &gorm.Config{Logger: logger.Default.LogMode(logger.Error)})
	return c.db, err
}

func (c *PostgreSQLConnector) GetDBInstance() *gorm.DB {
	return c.db
}

func (c *PostgreSQLConnector) CloseConnection() error {
	db := c.GetDBInstance()
	if db == nil {
		return fmt.Errorf("database connection is not opened")
	}

	var (
		sqlDB *sql.DB
		err   error
	)
	if sqlDB, err = db.DB(); err != nil {
		return err
	}

	return sqlDB.Close()
}
