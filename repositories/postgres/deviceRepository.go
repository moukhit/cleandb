package postgres

import (
	"cleandb/models"
	"errors"

	"gorm.io/gorm"
)

type PostgresDeviceRepository struct {
	DB *gorm.DB
}

func CreateDeviceRepository(db *gorm.DB) *PostgresDeviceRepository {
	return &PostgresDeviceRepository{DB: db}
}

func (repo *PostgresDeviceRepository) AddOrUpdate(model *models.DeviceModel) error {
	if repo.DB == nil {
		return errors.New("подключение к базе данных не инициализировано")
	}

	var (
		result *gorm.DB
		device *models.DeviceModel
	)

	result = repo.DB.First(&device, model.Imei)
	if result.Error == gorm.ErrRecordNotFound {
		result = repo.DB.Create(model)
	} else {
		device.SerialNumber1 = model.SerialNumber1
		device.DeviceType = model.DeviceType
		device.Tel = model.Tel
		device.Owner = model.Owner

		device.Firmware = model.Firmware
		device.SerialNumber2 = model.SerialNumber2
		device.VersionMajor = model.VersionMajor
		device.VersionMinor = model.VersionMinor
		device.VersionRevision = model.VersionRevision
		device.VersionPatch = model.VersionPatch
		device.VersionTest = model.VersionTest
		device.VoltageLimit = model.VoltageLimit
		device.BalanceSim1Limit = model.BalanceSim1Limit
		device.BalanceSim2Limit = model.BalanceSim2Limit
		device.EngStartPermit = model.EngStartPermit
		device.WebastoPermit = model.WebastoPermit
		device.GitHash = model.GitHash
		device.UidHash = model.UidHash

		result = repo.DB.Save(&device)
	}

	return result.Error
}

func (repo *PostgresDeviceRepository) Delete(imei uint64) error {
	if repo.DB == nil {
		return errors.New("подключение к базе данных не инициализировано")
	}

	result := repo.DB.Delete(&models.DeviceModel{}, imei)
	return result.Error
}

func (repo *PostgresDeviceRepository) GetByImei(imei uint64) (*models.DeviceModel, error) {
	if repo.DB == nil {
		return nil, errors.New("подключение к базе данных не инициализировано")
	}

	var (
		result  *gorm.DB
		pDevice *models.DeviceModel
	)

	pDevice = new(models.DeviceModel)
	result = repo.DB.First(pDevice, imei)
	return pDevice, result.Error
}
