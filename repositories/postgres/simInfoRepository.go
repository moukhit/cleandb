package postgres

import (
	"cleandb/models"
	"errors"

	"gorm.io/gorm"
)

type PostgresSimInfoRepository struct {
	DB *gorm.DB
}

func CreateSimInfoRepository(db *gorm.DB) *PostgresSimInfoRepository {
	return &PostgresSimInfoRepository{DB: db}
}

func (repo *PostgresSimInfoRepository) AddOrUpdate(model *models.SimInfoModel) error {
	if repo.DB == nil {
		return errors.New("подключение к базе данных не инициализировано")
	}

	var (
		result   *gorm.DB
		sim_info *models.SimInfoModel
	)

	result = repo.DB.Where(&models.SimInfoModel{
		Imei:  model.Imei,
		SimId: model.SimId,
		Mcc:   model.Mcc,
		Mnc:   model.Mnc,
	}).First(&sim_info)

	if result.Error == gorm.ErrRecordNotFound {
		result = repo.DB.Create(model)
	} else {
		sim_info.Imei = model.Imei
		sim_info.SimId = model.SimId
		sim_info.Mcc = model.Mcc
		sim_info.Mnc = model.Mnc
		sim_info.Phone = model.Phone

		result = repo.DB.Where(&models.SimInfoModel{
			Imei:  model.Imei,
			SimId: model.SimId,
			Mcc:   model.Mcc,
			Mnc:   model.Mnc,
		}).Save(&sim_info)
	}

	return result.Error
}

func (repo *PostgresSimInfoRepository) Delete(imei uint64, simid int32, mcc int32, mnc int32) error {
	if repo.DB == nil {
		return errors.New("подключение к базе данных не инициализировано")
	}

	result := repo.DB.Delete(&models.SimInfoModel{}, "imei=? and sim_id=? and mcc=? and mnc=?", imei, simid, mcc, mnc)
	return result.Error
}

func (repo *PostgresSimInfoRepository) Find(imei uint64, simid int32, mcc int32, mnc int32) (*models.SimInfoModel, error) {
	if repo.DB == nil {
		return nil, errors.New("подключение к базе данных не инициализировано")
	}

	var (
		result   *gorm.DB
		sim_info *models.SimInfoModel
	)

	result = repo.DB.Where(&models.SimInfoModel{
		Imei:  imei,
		SimId: simid,
		Mcc:   mcc,
		Mnc:   mnc,
	}).First(&sim_info)

	return sim_info, result.Error
}
